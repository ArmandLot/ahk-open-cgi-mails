# ahk - Open CGI Mails

>The true currency of life is time, not money, and we've all got a limited stock of that.
-Robert Harris.

## Description
I've made this script because I didn't want to loose X minutes a day opening my corporate mailbox through VPN while I was supposed to work for my client.

This script will auto-launch Firefox, copy-pasta IdentifyGuard Soft Token from your computer, then auto-magically process the identification steps to your Webmail.

***Please note:***

>1. Obviously, this breaks any rule of security you may have.
>2. Your security admin will hate you.
>3. Your manager will hate you.
>4. I don't care who you are, and as far as I know, you don't know me.
>5. Use at your own discretion, do whatever-you-want-with-the script.

## What you need to make it work
*Automation has it limits.*

1. Any version of Firefox (I use Developer version not to mess with my usual browser). [Get Firefox Quantum: Developer Edition here](https://www.mozilla.org/fr/firefox/developer/).
2. IdentifyGuard Soft Token installed on the very same PC (sorry if you installed it on your phone, but eh, your lose).
3. Install AutoHotKey. [Get AutoHotKey here](https://autohotkey.com/).

## Installation
**Step 1 | **
Install AutoHotKey.

**Step 2 | **
Pull the sources from master.
You should have a ahk-script file and a folder full of screenshots used to check each step of the script. *Go to
Troubleshooting if these images are not recognized on your computer*

**Step 3 | **
Complete the file named `credentials.ahk` in the same folder. **This file will contain your passwords *in plain text***. No security at all. *As I said, I DO NOT CARE about security. I care about my time.*

The `6` following variables must be updated *(by default)* :

```
FIREFOX_PATH
ENTRUST_PATH

USERNAME
MAIL_USERNAME
ENTRUST_PASSWORD
GATEWAY_PASSWORD
```

**Final Step | **
Right click and choose : "Compile script". This will generate a executable, just launch it. **If you give this file to anyone, say goodbye to your passwords**. *I don't care, and I don't want to know.*

## Troubleshooting
```
//TODO
```
