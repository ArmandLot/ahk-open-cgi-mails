﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
#SingleInstance force
#include, credentials.ahk
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Constants
WAIT_FOR_CHECK_INTERVAL=1000 ; check every x seconds
WAIT_FOR_MAX_CHECK_LOOPS=60  ; maximum count of checks
IMAGE_SEARCH_TOP_LEFT_X=0 ; x value of top left corner of search rect
IMAGE_SEARCH_TOP_LEFT_Y=0 ; y value of top left corner of search rect
IMAGE_SEARCH_BOTTOM_RIGHT_X=600 ; x value of bottom right corner of search rect
IMAGE_SEARCH_BOTTOM_RIGHT_Y=600 ; y value of bottom right corner of search rect
global IMAGE_FOUND_X = 0
global IMAGE_FOUND_Y = 0

Run, %FIREFOX_PATH%
Run, %ENTRUST_PATH%
WinWaitActive, Entrust IdentityGuard Token
WinActivate, Entrust IdentityGuard Token
;WinWaitActive, %GATEWAY_WINTITLE%
SendInput, %ENTRUST_PASSWORD%{Enter}
Send, ^c

Sleep, 2000
WinActivate, %GATEWAY_WINTITLE%
;;WaitForImage("res\au_auth.PNG")

;SendInput, %GATEWAY_URL% {Enter}
;WaitForImage("res\cgi.PNG")
Send, %USERNAME%
Send, {Tab}
Sleep, 500
Send, ^a
Send, {Delete}
SendInput, %GATEWAY_PASSWORD%
SendInput, ^v{Enter}

WaitForImageOrSession("res\mail.PNG")

WaitForImage("res\authent.PNG")
SendInput, %MAIL_USERNAME%

Send, {Tab}
Sleep, 100
Send {Enter}

WaitForImage("res\outlook.PNG")

IfWinExist, Entrust IdentityGuard Token
    WinClose ; use the window found above

MsgBox The true currency of life is time, not money, and we've all got a limited stock of that. -Robert Harris.

return

WaitForImage(filenameImage) {
  global WAIT_FOR_CHECK_INTERVAL
  global WAIT_FOR_MAX_CHECK_LOOPS

  global IMAGE_SEARCH_TOP_LEFT_X
  global IMAGE_SEARCH_TOP_LEFT_Y
  global IMAGE_SEARCH_BOTTOM_RIGHT_X
  global IMAGE_SEARCH_BOTTOM_RIGHT_Y

  loopCount = 0;

  Loop {
  	Sleep, %WAIT_FOR_CHECK_INTERVAL%
	CoordMode Pixel
  	ImageSearch, FoundX, FoundY, %IMAGE_SEARCH_TOP_LEFT_X%, %IMAGE_SEARCH_TOP_LEFT_Y%, %IMAGE_SEARCH_BOTTOM_RIGHT_X%, %IMAGE_SEARCH_BOTTOM_RIGHT_Y%, %filenameImage%
  	if (ErrorLevel = 0) {
		IMAGE_FOUND_X := FoundX + 10
		IMAGE_FOUND_Y := FoundY + 20
  		break ; image found
	}
  	loopCount += 1

  	if (loopCount > WAIT_FOR_MAX_CHECK_LOOPS) {
  		Msgbox Image %filenameImage% not found!
  		exitApp
  	}
  }
}

WaitForImageOrSession(filenameImage) {
  global WAIT_FOR_CHECK_INTERVAL
  global WAIT_FOR_MAX_CHECK_LOOPS

  global IMAGE_SEARCH_TOP_LEFT_X
  global IMAGE_SEARCH_TOP_LEFT_Y
  global IMAGE_SEARCH_BOTTOM_RIGHT_X
  global IMAGE_SEARCH_BOTTOM_RIGHT_Y

  loopCount = 0;

  Loop {
  	Sleep, %WAIT_FOR_CHECK_INTERVAL%
	CoordMode Pixel
  	ImageSearch, FoundX, FoundY, %IMAGE_SEARCH_TOP_LEFT_X%, %IMAGE_SEARCH_TOP_LEFT_Y%, %IMAGE_SEARCH_BOTTOM_RIGHT_X%, %IMAGE_SEARCH_BOTTOM_RIGHT_Y%, %filenameImage%
  	if (ErrorLevel = 0) {
		IMAGE_FOUND_X := FoundX + 10
		IMAGE_FOUND_Y := FoundY + 20
		MouseMove, IMAGE_FOUND_X, IMAGE_FOUND_Y
		Click, 1
  		break ; image found
	}
	ImageSearch, FoundX, FoundY, %IMAGE_SEARCH_TOP_LEFT_X%, %IMAGE_SEARCH_TOP_LEFT_Y%, %IMAGE_SEARCH_BOTTOM_RIGHT_X%, %IMAGE_SEARCH_BOTTOM_RIGHT_Y%, res\session.PNG
  	if (ErrorLevel = 0) {
		IMAGE_FOUND_X := FoundX + 10
		IMAGE_FOUND_Y := FoundY + 20
		MouseMove, IMAGE_FOUND_X, IMAGE_FOUND_Y
		Click, 1
	}
  	loopCount += 1

  	if (loopCount > WAIT_FOR_MAX_CHECK_LOOPS) {
  		Msgbox Image %filenameImage% not found!
  		exitApp
  	}
  }
}
