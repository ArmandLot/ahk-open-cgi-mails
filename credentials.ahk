;DON'T YOU EVER SHARE THIS FILE WITH ANYONE.
;Replace the values with your own. Copy/Paste links to directories and paswords without any quote.

;Applications
FIREFOX_PATH = PATH_TO_YOUR_FIREOX.EXE ; Ex.: C:\Programs\Firefox\firefox.exe
ENTRUST_PATH = PATH_TO_ENTRUST_SHORCUT ; May be on you desktop. Ex.: C:\Users\me\Desktop\IdentityGuard Soft Token
FIREFOX_APPNAME = Firefox Developer Edition ; Change this if you don't use ff Quantum to "Mozilla Firefox" (without quotes)
GATEWAY_URL = http://gateway6.cgi.com/ ; Default gateway link.
;Passwords
USERNAME = YOUR.USERNAME  ; First part of your mail address. Ex.: dominik.smith
MAIL_USERNAME = USERNAMEM; Should be lastname + first name's first letter. Ex.: smithd
ENTRUST_PASSWORD = YOUR_4_DIGITS_ENTRUST_PASSWORD ; Ex.: 1234
GATEWAY_PASSWORD = YOUR_10_DIGITS_GATEWAY_PASSWORD ; Ex.: 1234567890
